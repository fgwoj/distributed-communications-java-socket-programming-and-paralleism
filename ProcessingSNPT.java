public class ProcessingSNPT extends Thread {
    private volatile double value;
    private int threadNum;
    private int inputConvert;
    private double array;

    public ProcessingSNPT (int threadNum, int inputConvert, double array){
        this.threadNum = threadNum;
        this.inputConvert = inputConvert;
        this.array = array;
    }

    public static double power(double x){
		x =  Math.pow(2, x);
		return x;
	}

	public static double power2(double x){
		x = Math.pow(x,2);
		return x;
	}

	public static double root(double x){
		x = Math.sqrt(x);
		return x;
	}

	public static double loga(double x){
		x = Math.log(x);
		return x;
	}

    @Override
    public void run(){
        try{
            if(inputConvert == 5){
                double x = 0;
                //System.out.println(array);
    
            }else if(inputConvert == 4){
                // Thread.sleep(10000);

                double x = loga(array);
                //System.out.println(x + " " + threadNum);
                value = x;
    
            }else if(inputConvert == 3){
                //Thread.sleep(10000);

                double x = root(array);
                //System.out.println(x + " " + threadNum);
                value = x;
    
    
            }else if(inputConvert == 2){
                //Thread.sleep(10000);

                double x = power2(array);
                //System.out.println(x + " " + threadNum);
                value = x;
                
            }else if(inputConvert == 1){
                //Thread.sleep(10000); // Test timeout
                double x = power(array);
                //System.out.println(x + " Thread: " + threadNum);
                value = x;
            }
        }catch(Exception exception){

        }
        
    }  

    public double getValue(){
        return value;
    }
    public static void main(String[] args){

    }


}
