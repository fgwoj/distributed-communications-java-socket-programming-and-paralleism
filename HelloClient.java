import java.net.*;
import java.io.*;

public class HelloClient {

	private static int[] sequence;

	public static int[] addList(int x){
		return sequence;
	}
	public static void main(String[] args) throws IOException {
		
        if (args.length != 2) {
            System.err.println(
                "Usage: java HelloClient <host name> <port number>");
            System.exit(1);
        }

		String hostName = args[0];
		int portNumber = Integer.parseInt(args[1]);
		Socket myClientSocket = new Socket();
		myClientSocket.connect(new InetSocketAddress(hostName, portNumber),5000);
		
		try (
			PrintWriter output = new PrintWriter(myClientSocket.getOutputStream(),true);
			BufferedReader input = new BufferedReader(new InputStreamReader(myClientSocket.getInputStream()));
			BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
		) {
			myClientSocket.setSoTimeout(5000);
			System.out.println(input.readLine()); // initial welcome msg

			// ------ THREADS ------ //
			myClientSocket.setSoTimeout(5000);
			System.out.println(input.readLine());
			String newInput5 = stdIn.readLine(); // reads user's input
			//int inputConvert5 = Integer.parseInt(newInput5);  
			output.println(newInput5); // user's input transmitted to server
			// ------ END ------ //
			
			while(true){
				System.out.println(input.readLine());

				String newInput = stdIn.readLine(); // reads user's input
				int inputConvert = Integer.parseInt(newInput);  
				output.println(newInput); // user's input transmitted to server
				if(inputConvert == 5){
					//output.println(4);
					break;
				}else if(inputConvert == 4){
					while(true){
						System.out.println(input.readLine()); //Output server msg
						String newInput2 = stdIn.readLine(); // reads user's input
						int inputConvert2 = Integer.parseInt(newInput2);

						if(inputConvert2 == -1){
							output.println(newInput2); // user's input transmitted to server
							output.flush();
							break;
						}else{
							output.println(newInput2); // user's input transmitted to server
							output.flush();
						}
					}
					myClientSocket.setSoTimeout(5000);
					System.out.println(input.readLine()); //Output server msg
					System.out.println(input.readLine()); //Output server msg

					break;
				}else if(inputConvert == 3){
					while(true){
						System.out.println(input.readLine()); //Output server msg
						String newInput2 = stdIn.readLine(); // reads user's input
						int inputConvert2 = Integer.parseInt(newInput2);

						if(inputConvert2 == -1){
							output.println(newInput2); // user's input transmitted to server
							output.flush();
							break;
						}else{
							output.println(newInput2); // user's input transmitted to server
							output.flush();
						}
					}
					myClientSocket.setSoTimeout(5000);
					System.out.println(input.readLine()); //Output server msg
					System.out.println(input.readLine()); //Output server msg

					break;
				}else if(inputConvert == 2){
					while(true){
						System.out.println(input.readLine()); //Output server msg
						String newInput2 = stdIn.readLine(); // reads user's input
						int inputConvert2 = Integer.parseInt(newInput2);

						if(inputConvert2 == -1){
							output.println(newInput2); // user's input transmitted to server
							output.flush();
							break;
						}else{
							output.println(newInput2); // user's input transmitted to server
							output.flush();
						}
					}
					myClientSocket.setSoTimeout(5000);
					System.out.println(input.readLine()); //Output server msg
					System.out.println(input.readLine()); //Output server msg
					
					break;
				}else if(inputConvert == 1){
					while(true){
						myClientSocket.setSoTimeout(5000);
						System.out.println(input.readLine()); //Output server msg
						String newInput2 = stdIn.readLine(); // reads user's input
						int inputConvert2 = Integer.parseInt(newInput2);

						if(inputConvert2 == -1){
							output.println(newInput2); // user's input transmitted to server
							output.flush();
							break;
						}else{
							output.println(newInput2); // user's input transmitted to server
							output.flush();
						}
					}
					myClientSocket.setSoTimeout(5000);
					System.out.println(input.readLine()); //Output server msg
					System.out.println(input.readLine()); //Output server msg

					//System.out.println(input.readLine()); //Output server msg
					//String newInput3 = stdIn.readLine(); // reads user's input
					//output.println(newInput3); // user's input transmitted to server
					break;
				}
			}

		} catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                hostName);
			e.printStackTrace();
            System.exit(1);
        }
	
	}
}