#  Distributed Communications Java Socket Programming and Paralleism

## Introduction
This algorithm uses exactly one thread per number and k - |S| threads that are not used. The algorithm calculates every number from the list “operaration” and then puts it into another list called “divide”. The result is sent remotely to the client.

## Installation

1. Unzip file to desired location
2. In command line go to src location then insert this command: javac *.java
3. To run server open command line and type: java HelloServer
4. To run client where server is local, open another command line and type:  java HelloClient localhost 8000 
5. To run client where server is external, open another command line and type:  java HelloClient (IP address) 8000 
6. Follow the instructions in the program

