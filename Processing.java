import java.util.*;  

public class Processing extends Thread {
    private int threadNum;
    private int inputConvert;
    private List<Double> array;

    public Processing (int threadNum, int inputConvert, List<Double> array){
        this.threadNum = threadNum;
        this.inputConvert = inputConvert;
        this.array = array;
    }

    public static double power(double x){
		x =  Math.pow(2, x);
		return x;
	}

	public static double power2(double x){
		x = Math.pow(x,2);
		return x;
	}

	public static double root(double x){
		x = Math.sqrt(x);
		return x;
	}

	public static double loga(double x){
		x = Math.log(x);
		return x;
	}

    @Override
    public void run(){
        if(inputConvert == 5){
			double x = 0;
            System.out.println(array + " " + threadNum);

		}else if(inputConvert == 4){
            for(int i = 0; i < array.size(); i++){
                double x = loga(array.get(i));
                array.set(i, x);
            }
            System.out.println(array + " " + threadNum);

		}else if(inputConvert == 3){
            for(int i = 0; i < array.size(); i++){
			    double x = root(array.get(i));
                array.set(i, x);
            }
            System.out.println(array + " " + threadNum);

		}else if(inputConvert == 2){
            for(int i = 0; i < array.size(); i++){
			    double x = power2(array.get(i));
                array.set(i, x);
            }
            System.out.println(array + " " + threadNum);

		}else if(inputConvert == 1){
            for(int i = 0; i < array.size(); i++){
                double x = power(array.get(i));
                array.set(i, x);
            }
            System.out.println(array + " " + threadNum);
		}
    }  
    public static void main(String[] args){

    }


}
