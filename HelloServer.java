import java.net.*;
import java.io.*;
import java.util.*;  

public class HelloServer {
    private static long startTime;
	public static int message(String question, BufferedReader input, PrintWriter output, int inputConvert) throws IOException{
		output.println(question);
		output.flush();
		String inputLine2 = input.readLine();
		int inputConvert2 = Integer.parseInt(inputLine2);
		int x = inputConvert2;
		return x;
	}	
	// One thread per number 
	private static List<Double> SNPT(int threadNum, int sizeArray, List<Double> operation, int inputConvert) throws InterruptedException{
		List<Double> divide= new ArrayList<Double>();  
		
		int j = 0; // Index of the list
		for(int i = 0; i < threadNum; i++){
			if(i > sizeArray){
				i = 0; // Use the first threat of k
				sizeArray -=1;
			}else if(i == sizeArray){
				break;
			}
			ProcessingSNPT processing = new ProcessingSNPT(i, inputConvert, operation.get(j));
			j++;
			processing.start();
			processing.join();
			divide.add(processing.getValue());
		}
		return divide;
	}
	public static void main(String[] args) throws IOException, InterruptedException {
		
		int portNumber;
        if (args.length < 1) {
			System.out.println("Warning: You have provided no arguments\nTrying to connect to the default port 8000...");
			portNumber = 8000;
        } else if (args.length == 1) {
			portNumber = Integer.parseInt(args[0]);
		} else {
			System.out.println("Warning: You have provided > 1 arguments\nTrying with the first argument to connect to a port...");
			portNumber = Integer.parseInt(args[0]);
		}
		
		while(true){ //in order to serve multiple clients but sequentially, one after the other
			try (
				ServerSocket myServerSocket = new ServerSocket(portNumber);
				Socket aClientSocket = myServerSocket.accept();


				PrintWriter output = new PrintWriter(aClientSocket.getOutputStream(),true);
				BufferedReader input = new BufferedReader(new InputStreamReader(aClientSocket.getInputStream()));
			) {	
				List<Double> operation= new ArrayList<Double>();  

				System.out.println("Client " + aClientSocket.getInetAddress() + " has been connected.\n");
				output.println("You're connected to the server");
				output.flush();

				while(true){
					output.println("Limit of threads: ");
					output.flush();
					String inputLine3 = input.readLine();
					int threadNum = Integer.parseInt(inputLine3);
					System.out.println(threadNum);  

					output.println("Select option: 1. 2ˆx | 2. xˆ2 | 3. root(x) | 4. log(x)");
					output.flush();
					String inputLine = input.readLine();
					int inputConvert = Integer.parseInt(inputLine);  

					if(inputConvert == 5){ // log(x)
						System.out.println("The user is disconneted");
						break;
					}else if(inputConvert == 4){
						while(true){
							String question = "Enter x for log(x): ";
							double z = message(question, input, output, inputConvert);
							if(z == -1){
								break;
							}else{
								operation.add(z);
								System.out.println(operation); // Display list
							}
						}
						int sizeArray = operation.size();
					
						startTime = System.nanoTime();
						output.println(SNPT(threadNum, sizeArray, operation, inputConvert));
						long endTime   = System.nanoTime();
						long totalTime = endTime - startTime;
						System.out.println("Time: " + totalTime);

						output.flush();
						break;
					}else if(inputConvert == 3){ // root(x)
						while(true){
							String question = "Enter x for root(x): ";
							double z = message(question, input, output, inputConvert);
							if(z == -1){
								break;
							}else{
								operation.add(z);
								System.out.println(operation); // Display list
							}
						}
						int sizeArray = operation.size();
					
						startTime = System.nanoTime();
						output.println(SNPT(threadNum, sizeArray, operation, inputConvert));
						long endTime   = System.nanoTime();
						long totalTime = endTime - startTime;
						System.out.println("Time: " + totalTime);

						output.flush();
						break;
					}else if(inputConvert == 2){ // xˆ2
						while(true){
							String question = "Enter x for xˆ2: ";
							double z = message(question, input, output, inputConvert);
							if(z == -1){
								break;
							}else{
								operation.add(z);
								System.out.println(operation); // Display list
							}
						}

						int sizeArray = operation.size();
					
						startTime = System.nanoTime();
						output.println(SNPT(threadNum, sizeArray, operation, inputConvert));
						long endTime   = System.nanoTime();
						long totalTime = endTime - startTime;
						System.out.println("Time: " + totalTime);

						output.flush();

						break;
					}else if(inputConvert == 1){ // 2ˆx
						while(true){
							String question = "Enter x for 2ˆx (To finish type: -1): ";
							double z = message(question, input, output, inputConvert);
							if(z == -1){
								break;
							}else{
								operation.add(z);
								System.out.println(operation); // Display list
							}
						}
						int sizeArray = operation.size();

						int j = 0; // Index of the list
    					// One thread per number 
						startTime = System.nanoTime();
						output.println(SNPT(threadNum, sizeArray, operation, inputConvert));
						output.flush();
						long endTime   = System.nanoTime();
						long totalTime = endTime - startTime;
						output.println("Time: " + totalTime);

						output.flush();
						// Uniforly diving the sequence
						// System.out.println(operation.size());
						// System.out.println(operation);
						break;
					}		
				}	
			} catch (IOException e) {
				System.out.println("Exception caught when trying to listen on port "
				+ portNumber + " or listening for a connection");
				System.out.println(e.getMessage());			
			}
		}
	}
}